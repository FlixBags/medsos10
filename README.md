# Final project
## Kelompok 10
## Anggota kelompok
* Andre Christian Prasetyo
* Felix Bagus
* Reni Fatarina Handayani
## Tema project
Media sosial
## ERD
![ERD](https://gitlab.com/FlixBags/medsos10/-/raw/main/ERD.png)
## Link video
*Link demo aplikasi: [YouTube](https://www.youtube.com/watch?v=7S7ZQp8BxQ8)
*Link deploy: [Heroku](https://skypost10.herokuapp.com/)